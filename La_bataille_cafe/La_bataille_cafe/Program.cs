﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using La_bataille_cafe;

namespace Bataille_cafe
{
    public static class Program
    {
       

        static void Main(string[] args)
        {



            List<Parcelle> parcelles=new List<Parcelle>();
     
            Socket socket=SocketUse.Connection("192.168.0.12",1213);//51.91.120.237

            string trame= SocketUse.Reception(socket,true);  

      

            string[] entiers = new string[100];
            int[] Map = new int[100];

            Lecture_de_trame.Remplissage_Du_Tableau(entiers, trame);
            Map = Lecture_de_trame.Tab_To_Int(entiers);

            string[] decoder = new string[100];
            
            decoder=DecodageMap.Decodage(Map);

            Affichages.Affichage(decoder);

            int value = 97;
            for(int i =0;i<26; i++)
            {
                string lettre=char.ConvertFromUtf32(value);
                parcelles.Add(new Parcelle(lettre));
                for(int j = 0; j < 100; j++)
                {
                    if (decoder[j] == parcelles[i].lettre)
                    {
                        parcelles[i].AddCoordonnées(j);
                    }
                }
                parcelles[i].DefinePointGagnat();
                value = value + 1;
                
            }


            //Semestre 4 : debut 
            string validation;
            string choixia;
            string coupchoisis;
            string rejouer;
            choixia = "xx";
            do {

                coupchoisis = Play.choixducoup(choixia,parcelles);
                Console.WriteLine(coupchoisis);

                SocketUse.Envoi(socket, coupchoisis);

                validation= SocketUse.Reception(socket,false);
                //Console.WriteLine(validation);

                choixia = SocketUse.Reception(socket,false);
                Console.WriteLine(choixia);

                rejouer = SocketUse.Reception(socket,false);
                //Console.WriteLine(rejouer);
            }while(rejouer[0]=='E');

            
            SocketUse.Fermeture(socket);

            Console.ReadKey();
        }
    }
}
