﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Bataille_cafe
{
    public static class SocketUse
    {
        //Permet la connection au serveur distant
        public static Socket Connection(string ip, int port)
        {
            Socket monsocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                monsocket.Connect(ip, port); // "172.16.0.88", 1212  "51.91.120.237"
            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
            return monsocket;
        }

        //permet la reception de données du serveur distant
        public static string Reception(Socket monsocket)
        {
            byte[] tab = new byte[300];
            try
            {
                monsocket.Receive(tab);
                Console.WriteLine("reception reussi ?");
            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
            string decode = Encoding.UTF8.GetString(tab);
            return decode;
        }

        public static void Envoi(Socket monsocket,string envoi)
        {
            byte[] envoibyte = Encoding.Unicode.GetBytes(envoi);
            byte[] envoifinal = new byte[4];
            int i = 0;
            foreach (byte byt in envoibyte)
            {
                if (byt != 0)
                {
                    envoifinal[i] = byt;
                    i++;
                }
            }



            try
            {
                monsocket.Send(envoifinal);
                Console.WriteLine("envoi reussi ?");
               
                
            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
            
        }

        // ferme la connection au serveur 
        public static void Fermeture(Socket monsocket)
        {
            monsocket.Shutdown(SocketShutdown.Both);
            monsocket.Close();
        }
    }
}
