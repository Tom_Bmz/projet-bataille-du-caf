﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bataille_cafe;

namespace UnitTestBatailleCafe
{
    [TestClass]
    public class DecodageTest
    {
        [TestMethod]
        public void TestToBinaire()
        {
            int[] esperer = new int[4];
            esperer[0] = 1;
            esperer[1] = 1;
            esperer[2] = 0;
            esperer[3] = 0;
            int[] valeur_test = DecodageMap.ToBinaire(3);

            Assert.AreEqual(esperer[0], valeur_test[0]);
            Assert.AreEqual(esperer[1], valeur_test[1]);
            Assert.AreEqual(esperer[2], valeur_test[2]);
            Assert.AreEqual(esperer[3], valeur_test[3]);


            esperer[0] = 1;
            esperer[1] = 1;
            esperer[2] = 0;
            esperer[3] = 0;
            valeur_test = DecodageMap.ToBinaire(12);

            Assert.AreNotEqual(esperer[0], valeur_test[0]);
            Assert.AreNotEqual(esperer[1], valeur_test[1]);
            Assert.AreNotEqual(esperer[2], valeur_test[2]);
            Assert.AreNotEqual(esperer[3], valeur_test[3]);
        }

        [TestMethod]
        public void TestDecodage()
        {
            string[] decoder = new string[100];
            string[] test = new string[100];

            string trame = "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76 | 10:10:11:11:67:73:6:12:3:9 | 14:14:10:10:70:76:7:13:6:12 | 3:9:14:14:11:7:13:3:9:75 | 2:8:7:13:14:3:9:6:12:78 | 6:12:3:1:9:6:12:35:33:41 | 71:77:6:4:12:39:37:36:36:44 |";

            string[] entiers = new string[100];
            int[] Map = new int[100];

            Lecture_de_trame.Remplissage_Du_Tableau(entiers, trame);
            Map = Lecture_de_trame.Tab_To_Int(entiers);


            test[0] = "a";
            test[1] = "M";
            test[2] = "b";
            test[3] = "c";
            test[4] = "F";
            test[5] = "o";

            decoder = DecodageMap.Decodage(Map);

            Assert.AreEqual(decoder[0], test[0]);
            Assert.AreEqual(decoder[2], test[1]);
            Assert.AreEqual(decoder[6], test[1]);
            Assert.AreEqual(decoder[12], test[2]);
            Assert.AreEqual(decoder[24], test[3]);
            Assert.AreEqual(decoder[99], test[4]);
            Assert.AreEqual(decoder[72], test[5]);
            Assert.AreNotEqual(decoder[72], test[4]);
            Assert.AreNotEqual(decoder[0], test[4]);
            Assert.AreNotEqual(decoder[2], test[4]);
            Assert.AreNotEqual(decoder[12], test[4]);

        }
    }
}
