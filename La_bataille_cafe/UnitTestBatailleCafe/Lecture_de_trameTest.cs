﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bataille_cafe;

namespace UnitTestBatailleCafe
{
    [TestClass]
    public class Lecture_de_trameTest
    {
        [TestMethod]
        public void TestIs_chiffre()
        {
            string trame = "3: |9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|";
            Assert.IsTrue(Lecture_de_trame.Is_Chiffre(trame, 0));
            Assert.IsFalse(Lecture_de_trame.Is_Chiffre(trame, 1));
            Assert.IsFalse(Lecture_de_trame.Is_Chiffre(trame, 2));
            Assert.IsFalse(Lecture_de_trame.Is_Chiffre(trame, 3));
        }

        [TestMethod]
        public void TestRemplissage_du_tableau()
        {
            string trame1 = "22: 7: 8|78:2";


            string[] entiers = new string[100];
            string[] entiers_test = new string[100];

            entiers_test[0] = "22";
            entiers_test[1] = "7";
            entiers_test[2] = "8";
            entiers_test[3] = "78";
            entiers_test[4] = "2";

            Lecture_de_trame.Remplissage_Du_Tableau(entiers, trame1);
            Assert.AreEqual(entiers[0], entiers_test[0]);
            Assert.AreEqual(entiers[1], entiers_test[1]);
            Assert.AreEqual(entiers[2], entiers_test[2]);
            Assert.AreEqual(entiers[3], entiers_test[3]);
            Assert.AreNotEqual(entiers[4], "3");
        }


        [TestMethod]
        public void TestTab_to_int()
        {
            string[] strings = new string[100];
            int[] integers = new int[100];


            for (int k = 0; k < 100; k++)
            {
                strings[k] = "0";
                integers[k] = 0;

            }

            strings[0] = "22";
            strings[1] = "7";
            strings[2] = "8";
            strings[3] = "78";


            integers = Lecture_de_trame.Tab_To_Int(strings);


            Assert.AreEqual(22, integers[0]);
            Assert.AreEqual(integers[1], 7);
            Assert.AreEqual(integers[2], 8);
            Assert.AreNotEqual(integers[3], 79);

        }
    }
}
